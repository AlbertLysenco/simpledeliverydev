package ua.simpledelivery.service;

import ua.simpledelivery.entity.Client;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: albertlysenco
 * Date: 09.02.14
 * Time: 21:36
 * To change this template use File | Settings | File Templates.
 */
public interface ClientService {
    public void addClient(Client client);
    public List<Client> getAllClients();
    public void removeClient(Integer id);
}
