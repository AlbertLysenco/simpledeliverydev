package ua.simpledelivery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.simpledelivery.dao.ClientDao;
import ua.simpledelivery.entity.Client;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: albertlysenco
 * Date: 09.02.14
 * Time: 21:37
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    private ClientDao clientDao;

    @Transactional
    public void addClient(Client client) {
        clientDao.addClient(client);
    }

    @Transactional
    public List<Client> getAllClients() {
        return clientDao.getAllClients();
    }

    @Transactional
    public void removeClient(Integer id) {
        clientDao.removeClient(id);
    }
}
