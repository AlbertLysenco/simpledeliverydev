package ua.simpledelivery.entity;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: albertlysenco
 * Date: 09.02.14
 * Time: 20:36
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table (name = "client")
public class Client {
    @Id
    @GeneratedValue
    @Column (name = "CLIENT_ID")
    private int id;

    @Column (name = "CLIENT_NAME")
    private String name;

    @Column (name = "CLIENT_LASTNAME")
    private String lastName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
