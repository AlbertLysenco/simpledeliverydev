package ua.simpledelivery.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.simpledelivery.entity.Client;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: albertlysenco
 * Date: 09.02.14
 * Time: 20:57
 * To change this template use File | Settings | File Templates.
 */
@Repository
@Transactional
public class ClientDaoImpl implements ClientDao{

    @Autowired
    private SessionFactory sessionFactory;

    public void addClient(Client client){
        sessionFactory.getCurrentSession().save(client);
    }

    public List<Client> getAllClients(){
        return sessionFactory.getCurrentSession().createQuery("from Client").list();
    }

    public void removeClient(Integer id){
        sessionFactory.getCurrentSession().delete(id);
    }
}
