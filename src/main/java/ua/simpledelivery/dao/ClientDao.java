package ua.simpledelivery.dao;

import ua.simpledelivery.entity.Client;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: albertlysenco
 * Date: 09.02.14
 * Time: 20:56
 * To change this template use File | Settings | File Templates.
 */
public interface ClientDao {
    public void addClient(Client client);
    public List<Client> getAllClients();
    public void removeClient(Integer id);
}
